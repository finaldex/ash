const axios = require('axios')

const getPokeApi = async (route,ctx) => {
    return await axios
    .get(`https://pokeapi.co/api/v2/${route}`, { timeout: 30000 })
    .then(response => response.data)
    .catch(error => {
        ctx.reply(`Api service: ❌`)
    })
}

module.exports = getPokeApi