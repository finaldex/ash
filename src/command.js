const natural = require('natural')
const { removeStopwords, porBr } = require('stopword')
const pokemonNameList = require('../src/util/pokemons.json')

const tokenizer = new natural.WordTokenizer()

const getCommand = async (text) => {

    const tokenList = tokenizer.tokenize(text)
    const tokenListWithoutSW = removeStopwords(tokenList, porBr)

    if (tokenListWithoutSW.hasPokemon()) {
        return {
            name: "detalhes",
            endpoint: tokenListWithoutSW.filterPokemon()
        }
    } else if (tokenListWithoutSW.hasListCommand()) {
        return {
            name: "listar",
            endpoint: `?offset=${Math.floor(Math.random() * 1130)}&limit=10`
        }
    } else if (tokenListWithoutSW.hasStrongerCommand()) {
        return {
            name: "stronger",
            endpoint: `?offset=${Math.floor(Math.random() * 1130)}&limit=3`
        }
    } else{
        return {}
    }
}


Array.prototype.hasStrongerCommand = function () {
    return this.filter(item => item.includes('fort')).length > 0
}

Array.prototype.hasListCommand = function () {
    return this.filter(item => item.includes('list')).length >0
}

Array.prototype.hasPokemon = function () {
    return this.filter(item => pokemonNameList.includes(item)).length > 0
};

Array.prototype.filterPokemon = function () {
    return this.filter(item => pokemonNameList.includes(item))[0]
};

module.exports = getCommand