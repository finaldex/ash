const { Telegraf } = require('telegraf')

const bot = new Telegraf(process.env.BOT_TOKEN)

const getPokeApi = require('./service')
const getCommand = require('./command')

bot.on('text', async (ctx) => {

    const command = await getCommand(ctx.message.text.toLocaleLowerCase())
    console.log(command)
    try {
        const path = `pokemon/${command.endpoint}`
        const response = await getPokeApi(path, ctx)
        switch (command.name) {
            case "detalhes":
                getPokemonDetail(ctx, response)
                break
            case "listar":
                getPokemonList(ctx, response)
                break
            case "stronger":
                getPokemonDetail(ctx, response)
                break
            default:
                getError(ctx)
                break
        }

    } catch (error) {
        console.error(error)
    }
})

bot.on("text", ctx => ctx.reply(ctx.message))

const getError = async (ctx) => {
    ctx.reply(`Oi, eu sou o ash. Voce pode me pedir tres coisas:
    - Descrever um pokemon (precisa falar o nome dele)
    - Listar 10 pokemons
    - Citar os 3 pokemons mais fortes`)
}

const getStrongerPokemon = async (ctx, response) => {
    if (response != null) {
        ctx.reply(`esses sao os pokemons mais fortes`)
        response.results.map(item => ctx.reply(item.name))
    }
}

const getPokemonDetail = async (ctx, response) => {
    if (response != null) {
        ctx.replyWithPhoto(response.sprites.other.home.front_default)
        ctx.reply(`O pokemon e o ${response.name}`)
    }
}

const getPokemonList = async (ctx, response) => {
    if (response != null) {
        ctx.reply(`Aqui vai alguns exemplos de Pokemons. Voce pode saber mais sobre o pokemon perguntando o nome dele.`)
        response.results.map(item => ctx.reply(item.name))
    }
}

bot.launch()